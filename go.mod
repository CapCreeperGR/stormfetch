module stormfetch

go 1.22

require (
	github.com/BurntSushi/xgb v0.0.0-20210121224620-deaf085860bc
	github.com/jackmordaunt/ghw v1.0.4
	github.com/mitchellh/go-ps v1.0.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20240506104042-037f3cc74f2a // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/jackmordaunt/pcidb v1.0.1 // indirect
	github.com/jackmordaunt/wmi v1.2.4 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/moby/sys/mountinfo v0.7.1 // indirect
	golang.org/x/sys v0.3.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
